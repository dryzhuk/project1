Online shop template
--
- View site: http://dryzhuk.gitlab.io/project1/
- Version 1.0
- Author: Roman Dryzhuk 


## Configuration:

- Default
- GitLab CI: [`.gitlab-ci.yml`](https://gitlab.com/dryzhuk/project1/blob/master/.gitlab-ci.yml)

## More Info:
- https://t.me/dryzhuk